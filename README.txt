app README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/python setup.py develop

- $VENV/bin/initialize_app_db development.ini

- $VENV/bin/pserve development.ini


Run the tests with:
source /etc/default/huni
nosetests -v --with-cov --cov-report term-missing -s 
