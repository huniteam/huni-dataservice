$(document).ready(function() {
    //console.log('ready');

    $('#search_submit').click(function(e) {
        e.preventDefault();
        console.log('do search');
        var data = $('#search_form').serialize();
        console.log(data);
        data = data.replace('wt_format', 'wt');
        data = data.replace('search_input=', '');
        window.location = 'http://data.huni.net.au/solr/select?q=' + data;
    });
    
    $('#docid_input').keyup(function() {
        var empty = false;
        if ( $('#docid_input').val == '') {
                empty = true;
        };
        if (empty) {
            $('#docid_submit').attr('disabled', 'disabled');
        } else {
            $('#docid_submit').removeAttr('disabled');
        }
    });
    $('#docid_submit').click(function(e) {
        e.preventDefault();
        console.log('do docid');
        if ($('#docid_date').val() == '') {
            var url = [ $('#docid_input').val(), $('input[name=docid_options]:radio:checked').val() ].join('/');
        } else {
            var url = [ $('#docid_input').val(), $('input[name=docid_options]:radio:checked').val(), $('#docid_date').val() ].join('/');
        }
        window.location = '/' + url;
    });

    $("#statusTable").tablesorter( {
        sortList: [[0,0]],
        headers: {
            0: { sorter: false },
            2: { sorter: false }
        }
    });

    $('#previousReports').height(window.innerHeight - 150);
});