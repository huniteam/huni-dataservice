import unittest
import transaction
import shutil
import os
import os.path
from lxml import etree

from pyramid import testing
from datetime import datetime, date, timedelta

from .models import DBSession


class Set1(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_previous_reports(self):
        from .views import previous_reports
        request = testing.DummyRequest()
        result = previous_reports(request)
        self.assertEqual(type(result['reports']), tuple)
        self.assertEqual(len(result['reports']), 3)

    def test_harvest_status(self):
        # copy in our known test data files
        today = str(datetime.strftime(date.today(), "%Y-%m-%d"))    
        yesterday = str(date.today() - timedelta(days=1))
        
        if os.path.exists('/srv/aggregate/.status'):
            shutil.rmtree('/srv/aggregate/.status')
        os.mkdir('/srv/aggregate/.status')
        shutil.copy('test_data/status_today', "/srv/aggregate/.status/status_report_%s" % today)
        shutil.copy('test_data/status_yesterday', "/srv/aggregate/.status/status_report_%s" % yesterday)

        from .views import harvest_status
        request = testing.DummyRequest()
        result = harvest_status(request)

        # test that we get today's data
        self.assertEqual(result['today'], today)
        self.assertEqual(result['data'][0]['site'], 'ADB')
        self.assertEqual(result['data'][0]['clean'], '12588')

        # test that we get some other day's data
        request.matchdict['report'] = "status_report_%s" % yesterday
        result = harvest_status(request)
        self.assertEqual(result['today'], yesterday)
        self.assertEqual(result['data'][0]['site'], 'ADB')
        self.assertEqual(result['data'][0]['clean'], '588')

    def test_get_site_total(self):
        from .views import get_site_total

        site = 'ADB'
        result = get_site_total(site)
        self.assertEqual(result, 12588)
        
    def test_document(self):
        from .views import document
        request = testing.DummyRequest()
        #PDSC***item***SAW1-014/solr/2013-10-17

        # test we can get a known solr document
        request.matchdict['docid'] = 'PDSC***item***SAW1-014'
        request.matchdict['doctype'] = 'solr'
        result = document(request)
        doc = etree.fromstring(result['xml'])
        self.assertEqual(doc.xpath("//field[@name='type']")[0].text, "Artefact")
        self.assertEqual(doc.xpath("//field[@name='prov_doc_last_update']")[0].text, "2013-10-30T15:00:00Z")

        # test we can get a known original document
        request.matchdict['doctype'] = 'original'
        result = document(request)
        doc = etree.fromstring(result['xml'])
        self.assertEqual(doc.xpath("//dc:title", namespaces={ 'dc': 'http://purl.org/dc/elements/1.1/' })[0].text, "small tapes, Tahitian")

        # test we handle some other option gracefully
        request.matchdict['doctype'] = 'other'
        result = document(request)
        doc = etree.fromstring(result['xml'])
        self.assertEqual(doc.xpath("//result")[0].text, 'No result found')

        # test we can get a known earlier solr document
        request.matchdict['doctype'] = 'solr'
        request.matchdict['version'] = '2013-10-17'
        result = document(request)
        doc = etree.fromstring(result['xml'])
        self.assertEqual(doc.xpath("//field[@name='prov_doc_last_update']")[0].text, "2013-10-30T14:00:00Z")

        # test we can get the original document
        request.matchdict['doctype'] = 'original'
        result = document(request)
        doc = etree.fromstring(result['xml'])
        self.assertEqual(doc.xpath("//dc:title", namespaces={ 'dc': 'http://purl.org/dc/elements/1.1/' })[0].text, "small tapes, Tahitian")

        # test we can't get an unknown document
        request.matchdict['docid'] = 'NO***SUCH***DOC'
        request.matchdict['doctype'] = 'solr'
        result = document(request)
        doc = etree.fromstring(result['xml'])
        self.assertEqual(doc.xpath("//result")[0].text, 'No result found')

        # test we can't get a non existent earlier version
        request.matchdict['docid'] = 'PDSC***item***SAW1-014'
        request.matchdict['doctype'] = 'solr'
        request.matchdict['version'] = '1980-10-17'
        result = document(request)
        doc = etree.fromstring(result['xml'])
        self.assertEqual(doc.xpath("//result")[0].text, 'No result found')
