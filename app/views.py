from pyramid.response import Response
from pyramid.view import view_config
from pyramid.view import notfound_view_config
from pyramid.httpexceptions import HTTPNotFound

from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    MyModel,
    )

from lxml import etree
import glob
import os.path
import logging
import json
import requests
import urlparse
from datetime import datetime, date
log = logging.getLogger('app')

AGGREGATE="/srv/aggregate"

@notfound_view_config(renderer="404.mak")
def notfound(req):
    """404 not found view"""
    return {}

@view_config(route_name='home', renderer='index.mak')
def index(request):
    """Front page
    
    No dynamic content here - just a one page jquery app to assemble the request
    and trigger a GET on Solr. 
    """
    return {}

@view_config(route_name="api_help", renderer='api_help.mak')
def api_help(request):
    """API help page"""
    return {}

@view_config(route_name="previous_reports", renderer="previous-reports.mak")
def previous_reports(request):
    """Assemble the previous reports page
    
    This is linked from the /status endpoint.
    """
    reports = [ os.path.split(f) for f in sorted(glob.glob(os.path.join(AGGREGATE, '.status/status_report_????-??-??')))]
    reports = [ r + (r[1].replace('_', ' '),) for r in reversed(sorted(reports)) ]
    
    # we want 3 
    nPerList = len(reports)/ 3 + 1
    c1 = reports[0:nPerList]
    c2 = reports[nPerList:nPerList*2]
    c3 = reports[nPerList*2:]
    return { 'reports': (c1, c2,c3) }

@view_config(route_name="harvest_status", renderer="status.mak")
@view_config(route_name="harvest_status_earlier", renderer="status.mak")
def harvest_status(request):
    """Assemble today's status data object

    if request.matchdict['report'] is not set then find today's datafile
        and construct the status from that.
    else
        if request.matchdict['report'] = status_report_YYYY-mm-dd then we're looking
            for a previous report

    @params:
    - request.matchdict['report'] = 'status_report_YYYY-MM-DD'
    """
    try:
        report = request.matchdict['report']
        data = os.path.join(AGGREGATE, ".status", report)
        today = datetime.strptime(report.split('report_')[1], '%Y-%m-%d')
        today_human = str(datetime.strftime(today, "%B %d, %Y"))
        today = str(datetime.strftime(today, "%Y-%m-%d"))
    except KeyError:
        data = os.path.join(AGGREGATE, '.status.txt')
        today_human = str(datetime.strftime(date.today(), "%B %d, %Y"))
        today = str(datetime.strftime(date.today(), "%Y-%m-%d"))
    f = open(data, 'r')
    data = []
    i = 0
    for line in f:
        i += 1
        (site, orig, clean, invalid, solr, last) = line.split()
        stats = {}
        stats['number'] = i
        stats['site'] = site.strip(',')
        stats['total'] = get_site_total(stats['site'])
        stats['original'] = orig.strip(',')
        stats['clean' ] = clean.strip(',')
        stats['invalid'] = invalid.strip(',')
        stats['solr'] = solr.strip(',')
        stats['last'] = last.strip(',')
        data.append(stats)
    return { 
        'data': data, 
        'today': today,
        'today_human': today_human
    }

def get_site_total(site):
    """ Given a site code (e.g. ADB), return the total number of docs in the index
    
    @params:
    - site: ADB
    """
    query = "http://data.huni.net.au/solr/select?q=sourceAgencyCode:%s&rows=0&wt=json" % site
    data = do_search(query)
    if data is not None:
        numFound = data['response']['numFound']
        return numFound 
    return 0

@view_config(route_name="document_route1", renderer='xml.mak')
@view_config(route_name="document_route2", renderer='xml.mak')
def document(request):
    """Retrieve a document

    Given a document identifier and potentially a type and version, retrieve
    the specified XML document and return it as XML

    @params:
    - request.matchdict['docid']
    - request.matchdict['doctype']
    - request.matchdict['version']
    """
    request.response.content_type = "text/xml"

    docid = request.matchdict['docid']
    doctype = request.matchdict['doctype']
    try:
        version = request.matchdict['version']
        try:
            datevalue = datetime.strptime(version, "%Y-%m-%d")
        except:
           raise HTTPNotFound
    except KeyError:
        version = None
    log.debug("Request: %s, %s, %s" % (docid, doctype, version))

    if version is not None:
        document = get_version(docid, version, doctype)
        if document is None:
            return { 'xml': '<result>No result found</result>' }
    else:   
        if doctype == 'solr':
            document = get_solr_document(docid)
        elif doctype == 'original':
            document = get_original_document(docid)
        else:
            return { 'xml': '<result>No result found</result>' }

    if document is not None:
        return { 'xml': get_document(document) }
    return { 'xml': '<result>No result found</result>' } 

def get_document(document):
    """GET a document

    Returns the text data.

    @params:
    - document: a URL 
    """
    response = requests.get(document)
    if response.status_code != 200:
        log.error("get_document: failure: %s" % response.status_code)
        return None
    return response.text

def do_search(query):
    """GET a query

    Loads and returns the JSON response: use this method to perform
    a solr query.

    @params:
    - query: a URL
    """
    response = requests.get(query)
    if response.status_code != 200:
        log.error("oops - something went wrong: %s" % response.status_code)
        return None
    return json.loads(response.text)

def get_solr_document(docid):
    history = get_history(docid) 
    if history is not None:
        return history.pop()
    return None

def get_history(docid):
    query = "http://data.huni.net.au/solr/select?q=docid:%s&fl=document_history&wt=json" % docid
    data = do_search(query)
    if data is not None and len(data['response']['docs']) > 0:
        history = data['response']['docs'][0]['document_history']
        return history
    return None
            
def get_original_document(docid):
    query = "http://data.huni.net.au/solr/select?q=docid:%s&fl=provider_source&wt=json" % docid
    data = do_search(query)
    if data is not None:
        document = data['response']['docs'][0]['provider_source']
        return document
    return None

def get_version(docid, version, doctype):
    document = None

    history = get_history(docid)
    if history is not None:
        for h in history:
            urlobj = urlparse.urlparse(h)
            urlobj = urlobj.path.split('/')[1:]
            if version == urlobj[1]:
                document = h
                break

    if document is not None:
        doc = etree.parse(document)
        if doctype == 'solr':
            return doc.xpath("//*[local-name()='field' and @*[local-name()='name']='document_history']")[0].text
        elif doctype =='original':
            return doc.xpath("//*[local-name()='field' and @*[local-name()='name']='provider_source']")[0].text
        else:
            return None



