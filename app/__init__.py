from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from .models import (
    DBSession,
    Base,
    )


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)
    #config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('api_help', '/api_help')
    config.add_route('harvest_status', '/status')
    config.add_route('harvest_status_earlier', '/status/{report}')
    config.add_route('document_route1', '/{docid}/{doctype}')
    config.add_route('document_route2', '/{docid}/{doctype}/{version}')
    config.add_route('previous_reports', '/previous-reports')
    config.scan()
    return config.make_wsgi_app()
