<%inherit file="base.mak"/>
<%block name="content">
	<div class="container">
		<div class="row">
			<div class="page-header text-center">
				<h2><img src="/static/images/huni-logo-21.png">&nbsp;data&nbsp;service</h2>
				<div class="row" style="padding-left: 30px; padding-right: 30px;">
				</div>
			</div>
		</div>
		<div class="row text-center">
			We can't match that URL. Check that you've provided it in the form:
			<br/>
			<br/>
			<h3>/(SOLR DOCUMENT ID)/[solr | original]/[YYYY-MM-DD]</h3>
			<br/>
			<br/>
			Otherwise try using the <a href="/">front page app</a>
		</div>
	</div> <!-- /container -->
</%block>

