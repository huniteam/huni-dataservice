<%inherit file="base.mak"/>
<%block name="content">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="container">
        <div class="row">
            <div class="page-header text-center">
                <h2><img src="/static/images/huni-logo-21.png">&nbsp;harvest&nbsp;report&nbsp;</h2>
                <div class="row" style="padding-left: 30px; padding-right: 30px;">
                    <a class="pull-left" href="/status">back</a>
                </div>
            </div>
        </div>
        <div class="row" id="previousReports" style="overflow: auto;">
			<div class="col-md-4">
				<ul>
					%for r in reports[0]:
						<li><a href="/status/${r[1]}">${r[2]}</a></li>
					%endfor
				</ul>
			</div>
			<div class="col-md-4">
				<ul>
					%for r in reports[1]:
						<li><a href="/status/${r[1]}">${r[2]}</a></li>
					%endfor
				</ul>
			</div>
			<div class="col-md-4">
				<ul>
					%for r in reports[2]:
						<li><a href="/status/${r[1]}">${r[2]}</a></li>
					%endfor
				</ul>
			</div>
        </div>
</%block>

