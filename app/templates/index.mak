<%inherit file="base.mak"/>
<%block name="content">
	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="container">
		<div class="row">
			<div class="page-header text-center">
				<h2><img src="/static/images/huni-logo-21.png">&nbsp;data&nbsp;service</h2>
				<div class="row" style="padding-left: 30px; padding-right: 30px;">
					<span class="pull-left"> Jump to the
						<a target="_blank" href="http://data.huni.net.au/aggregate">aggregate</a> or
						<a target="_blank" href="http://data.huni.net.au/clean">clean</a> data
					</span>
					<a class="" href="/status">Harvest Status</a>
					<a class="pull-right" href="/api_help" target="_blank">API help</a>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<p>The HuNI data service is intended to ease the lookup of information in the aggregate.</p>
			<p>A complement to the Solr search service 
				(<a href="http://data.huni.net.au/solr/select" target="_blank">http://data.huni.net.au/solr/select</a>),<br/> 
				the data service provides an API to retrieve data attached to a solr document identifier.
			</p>
		</div>
		<hr/>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">Type in a Solr query</div>
					<div class="panel-body">
						<form role="form" id="search_form">
							<div class="form-group">
								http://data.huni.net.au/solr/select?q=
								<input type="search" name="search_input" class="form-control" placeholder="e.g. *:*" value="*:*">
							</div>
							<div class="form-group">
								<p>Select the output format for the search.</p>
								<div class="radio">
								  <label>
									<input type="radio" name="wt_format" id="wt_format" value="json" checked>
									JSON
								  </label>
								</div>
								<div class="radio">
								  <label>
									<input type="radio" name="wt_format" id="wt_format" value="xml">
									XML
								  </label>
								</div>
								<div class="radio">
								  <label>
								   <input type="radio" name="wt_format" id="wt_format" value="csv">
								   CSV 
								  </label>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-default" type="submit" id="search_submit">Search</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">Type in a Solr document identifier</div>
					<div class="panel-body">
						<form role="form" id="docid_form">
							<div class="form-group">
								<input type="search" id="docid_input" class="form-control" placeholder="e.g. PDSC***item***SAW1-014">
							</div>
							<div class="form-group">
								<p>Select which document to retrieve</p>
								<div class="radio">
								  <label>
									<input type="radio" name="docid_options" value="solr" checked>
									Solr: return the most recent version of the solr document used to create the record
								  </label>
								</div>
								<div class="radio">
								  <label>
									<input type="radio" name="docid_options" value="original">
									Original: return the most recent version of the original document as HuNI saw it
								  </label>
								</div>
							</div>
							<div class="form-group">
								<p>Enter the date of the version you require.</p>
								<input type="date" id="docid_date" class="form-control">
							</div>
							<div class="form-group">
								<button class="btn btn-default" type="submit" id="docid_submit" disabled="disabled">Search</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- /container -->
</%block>

