<%inherit file="base.mak"/>
<%block name="content">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="container">
        <div class="row">
            <div class="page-header text-center">
                <h2><img src="/static/images/huni-logo-21.png">&nbsp;harvest&nbsp;report&nbsp;</h2>
				<h4>${today_human}</h4>
                <div class="row" style="padding-left: 30px; padding-right: 30px;">
                    <a class="pull-left" href="/">Home</a>
                	<a class="pull-right" href="/previous-reports">View earlier report</a>
                </div>
            </div>
        </div>
        <div class="row">
			<p>
				Statistics from the last harvest run on <strong>${today_human}</strong>. 
				<a class="pull-right" href="http://data.huni.net.au/feed/updates.xml"><img src="/static/images/rss.jpg" style="height: 30px;"/>Subscribe to updates</a>
			</p>
			<h4>Legend</h4>
			<ul>
				<li>Rows coloured <span class="text-success">green</span> indicate sites with new data that was harvested today.</li>
				<li>Rows coloured <span class="text-danger">red</span> indicate sites with invalid data.</li>
			</ul>
			<hr/>
			<table class="tablesorter table table-condensed" id="statusTable">
				<thead>
					<tr>
						<tr>
							<th colspan="3"></th>
							<th colspan="4" class="text-center">Today's harvest</th>
							<th colspan="1" class="text-center"></th>
						</tr>
						<tr>
							<th class="text-center"></th>
							<th class="text-center">Site</th>
							<th class="text-center">Total In Index</th>
							<th class="text-center">Originals</th>
							<th class="text-center">Clean</th>
							<th class="text-center">Invalid</th>
							<th class="text-center">Solr</th>
							<th class="text-center">Last Harvest</th>
						</tr>
					</tr>
				</thead>
				<tbody>
				%for d in data:
					%if int(d['invalid']) > 0:
					<tr class="danger">
					%elif d['last'] == today:
					<tr class="success">
					%else:
					<tr>
					%endif
						<td class="text-center">${d['number']}</td>
						<td class="text-center">${d['site']}</td>
						<td class="text-center">${d['total']}</td>
						<td class="text-center">
							%if int(d['original']) > 0:
							<a href="/aggregate/${d['last']}/${d['site']}/original">${d['original']}</a>
							%else:
							${d['original']}
							%endif
						</td>
						<td class="text-center">
							%if int(d['clean']) > 0:
							<a href="/aggregate/${d['last']}/${d['site']}/clean">${d['clean']}</a>
							%else:
							${d['clean']}
							%endif
						</td>
						<td class="text-center">
							%if int(d['invalid']) > 0:
							<a href="/aggregate/${d['last']}/${d['site']}/invalid">${d['invalid']}</a>
							%else:
							${d['invalid']}
							%endif
						</td>
						<td class="text-center">
							%if int(d['solr']) > 0:
							<a href="/aggregate/${d['last']}/${d['site']}/solr">${d['solr']}</a>
							%else:
							${d['solr']}
							%endif
						</td>
						<td class="text-center">
							<a href="/aggregate/${d['last']}/${d['site']}">${d['last']}</a>
						</td>
					</tr>
				%endfor
				</tbody>
			</table>
        </div>
</%block>

