<%inherit file="base.mak"/>
<%block name="content">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="container">
        <div class="row">
            <div class="page-header text-center">
                <h2><img src="/static/images/huni-logo-21.png">&nbsp;data&nbsp;service</h2>
                <div class="row" style="padding-left: 30px; padding-right: 30px;">
                    <a class="pull-left" href="/">Home</a>
                </div>
            </div>
        </div>
        <div class="row">
		<p>
			The data service API is intentionally simple. The overarching goal was to ease the retrieval of various
			document versions in the index. The form of the API is:
			<h4 class="text-center">/(SOLR DOCUMENT ID)/[solr || original]/[DATE in the form YYYY-MM-DD]</h4>
		</p>
		<p>
			Q: How would I use this service?<br/>
			A: Perform your search against the Solr index (http://data.huni.net.au/solr/select) to find documents
			of interest then use this service to retrieve the original content and / or earlier versions if they
			exist.
		</p>
			
		<hr/>
		<p>
			Given a Solr document identifier of <strong>PDSC***item***SAW1-014</strong>:
		</p>
		<ul>
			<li>Retrieve the most recent Solr document generated from the data providers source</li>
		  	<ul>
				<li><a href="/PDSC***item***SAW1-014/solr" target="_blank">/PDSC***item***SAW1-014/solr</a></li>
		  	</ul>
			<br/>
			<li>Retrieve the most recent original document as observed by HuNI</li>
		  	<ul>
				<li><a href="/PDSC***item***SAW1-014/original" target="_blank">/PDSC***item***SAW1-014/original</a></li>
		  	</ul>
			<br/>
			<li>Retrieve the original document as observed by HuNI on the 17th October, 2013</li>
		  	<ul>
				<li><a href="/PDSC***item***SAW1-014/original/2013-10-17" target="_blank">/PDSC***item***SAW1-014/original/2013-10-17</a></li>
		  	</ul>
			<br/>
			<li>Retrieve the solr document from the 17th October, 2013</li>
		  	<ul>
				<li><a href="/PDSC***item***SAW1-014/solr/2013-10-17" target="_blank">/PDSC***item***SAW1-014/solr/2013-10-17</a></li>
		  	</ul>
		</ul>
		
        </div>
</%block>

